# See
# https://github.com/intuit/karate#syntax-guide
# for how to write feature scenarios
Feature: As an api user I want to retrieve some spring boot repo

   Scenario: Get 4 the old users
    Given url microserviceUrl
    And path 'olduser/'
    Given param numberOfAccountToReturn = 4
    When method GET
    Then status 200
    And match header Content-Type contains 'application/json'
    # see https://github.com/intuit/karate#schema-validation
    # Define the required schema
    * def oldUserSchema = { id : '#number' , html_url : '##string', login : '#string' }
    # The response should have an array of 4 objects
    And match response == '#[4] oldUserSchema' 
    
   Scenario: Get 10 the old users
    Given url microserviceUrl
    And path 'olduser/'
    Given param numberOfAccountToReturn = 10
    When method GET
    Then status 200
    And match header Content-Type contains 'application/json'
    # see https://github.com/intuit/karate#schema-validation
    # Define the required schema
    * def oldUserSchema = { id : '#number' , html_url : '##string', login : '#string' }
    # The response should have an array of 10 objects
    And match response == '#[10] oldUserSchema'
    
    
   Scenario: Get 1 the old users by default when no query parameter is passed
    Given url microserviceUrl
    And path 'olduser/'
    When method GET
    Then status 200
    And match header Content-Type contains 'application/json'
    # see https://github.com/intuit/karate#schema-validation
    # Define the required schema
    * def oldUserSchema = { id : '#number' , html_url : '##string', login : '#string' }
    # The response should have an array of 1 objects
    And match response == '#[1] oldUserSchema'

   Scenario: Get error message when wrong query parameter with valid data is passed.
    Given url microserviceUrl
    And path 'olduser/'
    When method GET
    Then status 200
    And match header Content-Type contains 'application/json'
    # see https://github.com/intuit/karate#schema-validation
    # Define the required schema
    * def oldUserSchema = { id : '#number' , html_url : '##string', login : '#string' }
    # The response should have an array of 1 objects
    And match response == '#[1] oldUserSchema'
    
   Scenario: Get error message when inavalid query parameter is passed.
    Given url microserviceUrl
    And path 'olduser/'
    Given param numberOfAccountToReturn = -1
    When method GET
    Then status 400
    And match header Content-Type contains 'application/json'
    # see https://github.com/intuit/karate#schema-validation
    # Define the required schema
    * def errorSchema = { timestamp : '#number' , status : '400', error : 'Bad Request', message: '#string' , path : '#string' }
    And match response == 
    """
    { 
      timestamp : '#notnull',
      status : 400,
      error : 'Bad Request',
      message: '#string' ,
      path : '#string' 
    }
    """
    