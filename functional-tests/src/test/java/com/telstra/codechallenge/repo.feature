# See
# https://github.com/intuit/karate#syntax-guide
# for how to write feature scenarios
Feature: As an api user I want to retrieve some spring boot repo

   Scenario: Get 9 the repo
    Given url microserviceUrl
    And path 'repo/'
    Given param numberOfRepoToReturn = 9
    When method GET
    Then status 200
    And match header Content-Type contains 'application/json'
    # see https://github.com/intuit/karate#schema-validation
    # Define the required schema
    * def repoSchema = { html_url : '#string' , watchers_count : '##number', name : '#string' , language : '##string',description : '##string'}
    # The response should have an array of 9 objects
    And match response == '#[9] repoSchema' 

   Scenario: Get 10 the repo by default when no query parameter is passed
    Given url microserviceUrl
    And path 'repo/'
    When method GET
    Then status 200
    And match header Content-Type contains 'application/json'
    # see https://github.com/intuit/karate#schema-validation
    # Define the required schema
    * def repoSchema = { html_url : '#string' , watchers_count : '##number', name : '#string' , language : '##string',description : '##string'}
    # The response repo should have an array of 10 objects
    And match response == '#[10] repoSchema' 
    
   Scenario: Get error message when wrong query parameter with valid data is passed.
    Given url microserviceUrl
    And path 'repo/'
    When method GET
    Then status 200
    And match header Content-Type contains 'application/json'
    # see https://github.com/intuit/karate#schema-validation
    # Define the required schema
    * def repoSchema = { html_url : '#string' , watchers_count : '##number', name : '#string' , language : '##string',description : '##string'}
    # The response repo should have an array of 10 objects
    And match response == '#[10] repoSchema' 
    
  
   Scenario: Get error message when inavalid query parameter is passed.
    Given url microserviceUrl
    And path 'repo/'
    Given param numberOfRepoToReturn = -1
    When method GET
    Then status 400
    And match header Content-Type contains 'application/json'
    # see https://github.com/intuit/karate#schema-validation
    # Define the required schema
    * def errorSchema = { timestamp : '#number' , status : '400', error : 'Bad Request', message: '#string' , path : '#string' }
    And match response == 
    """
    { 
      timestamp : '#notnull',
      status : 400,
      error : 'Bad Request',
      message: '#string' ,
      path : '#string' 
    }
    """