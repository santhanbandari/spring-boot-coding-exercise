package com.telstra.codechallenge.exceptions;

public class InvalidNumberOfAccountException extends Exception{
	public InvalidNumberOfAccountException(String message){
		super(message);
	}
}
