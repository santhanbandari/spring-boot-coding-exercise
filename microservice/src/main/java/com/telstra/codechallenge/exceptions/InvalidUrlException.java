package com.telstra.codechallenge.exceptions;

public class InvalidUrlException extends Exception{
	public InvalidUrlException(String message){
		super(message);
	}
}
