package com.telstra.codechallenge.Response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class ResponseOldUserDTO {
	
	private Long id;
	private String html_url;
	private String login;
	
}
