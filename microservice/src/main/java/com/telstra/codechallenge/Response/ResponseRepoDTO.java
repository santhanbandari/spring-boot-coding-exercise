package com.telstra.codechallenge.Response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class ResponseRepoDTO {
	
	private Long watchers_count;
	private String html_url;
	private String name;
	private String language;
	private String description;
	
	@Override
	public String toString() {
		return "ResponseHotDTO [html_url=" + html_url + ", watchers_count=" + watchers_count + ", name=" + name
				+ ", language=" + language + ", description=" + description + "]";
	}
	
	
}
