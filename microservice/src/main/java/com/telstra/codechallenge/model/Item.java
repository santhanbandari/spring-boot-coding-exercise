package com.telstra.codechallenge.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@JsonIgnoreProperties(ignoreUnknown = true)
@Getter @Setter @NoArgsConstructor
public class Item {
	
	
	private String html_url;
	private Long watchers_count;
	private String name;
	private String language;
	private String description;
	private Long id;
	private String login;
	
	
	
	public Item(String html_url, Long watchers_count, String name, String language, String description) {
		super();
		this.html_url = html_url;
		this.watchers_count = watchers_count;
		this.name = name;
		this.language = language;
		this.description = description;
	}

	@Override
	public String toString() {
		return "Item [html_url=" + html_url + ", watchers_count=" + watchers_count + ", name=" + name + ", language="
				+ language + ", description=" + description + ", id=" + id + ", login=" + login + "]";
	}

	
	
}
