package com.telstra.codechallenge.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter @Setter @NoArgsConstructor
public class User {
	
	private Long total_count;
	private String incomplete_results;
	private List<Item> items;
	
	@Override
	public String toString() {
		return "Item [total_count=" + total_count + ", incomplete_results=" + incomplete_results + ", item=" + items
				+ "]";
	}
	
}
