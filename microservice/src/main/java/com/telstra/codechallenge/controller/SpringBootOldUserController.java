package com.telstra.codechallenge.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.telstra.codechallenge.Response.ResponseOldUserDTO;
import com.telstra.codechallenge.exceptions.InvalidNumberOfAccountException;
import com.telstra.codechallenge.exceptions.InvalidUrlException;
import com.telstra.codechallenge.service.SpringBootOldUserService;
import com.telstra.codechallenge.util.Constants;

/**
 * Old User Controller  
 * @author Santhan.Bandari
 *
 */
@RestController
public class SpringBootOldUserController {
	private static final Logger mLOGGER = Logger.getLogger(SpringBootOldUserController.class);
	private SpringBootOldUserService springBootOldUserService;

	public SpringBootOldUserController(
			SpringBootOldUserService springBootOldUserService) {
		this.springBootOldUserService = springBootOldUserService;
	}
	
	/**
	 * Get Old user accounts with Zero Followers
	 * 
	 * @param numberOfAccountToReturn default value 1
	 * @return list oldest user accounts with zero followers
	 */

	@RequestMapping(path = Constants.USER_ENDPOINT, method = RequestMethod.GET)
	public ResponseEntity<List<ResponseOldUserDTO>> getOldUserRepo(@RequestParam(value = Constants.USER_PARAM, defaultValue = "1") int numberOfAccountToReturn) {
		try {
            return ResponseEntity.ok().body(springBootOldUserService.getOldUserList(numberOfAccountToReturn));
        }catch (InvalidNumberOfAccountException e) {
        	mLOGGER.error("Exception Occured in getOldUserRepo: "+e.getMessage());
        	mLOGGER.debug("Bad Request: ");
        	throw new ResponseStatusException(
      	          HttpStatus.BAD_REQUEST, e.getMessage());
        }catch(Exception e) {
        	mLOGGER.error("Exception Occured in getOldUserRepo: "+e.getMessage());
        	mLOGGER.debug("Internal Server Error: ");
        	throw new ResponseStatusException(
      	          HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        }
	}
	
}