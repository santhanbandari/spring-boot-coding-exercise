package com.telstra.codechallenge.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.telstra.codechallenge.Response.ResponseRepoDTO;
import com.telstra.codechallenge.exceptions.InvalidNumberOfAccountException;
import com.telstra.codechallenge.service.SpringBootRepoService;
import com.telstra.codechallenge.util.Constants;

/**
 * 
 * SpringBootRepoController
 * 
 * @author Santhan.Bandari
 *
 */
@RestController
public class SpringBootRepoController {
  private static final Logger mLOGGER = Logger.getLogger(SpringBootRepoController.class);
 
  private SpringBootRepoService springBootRepoService;

  public SpringBootRepoController(
		  SpringBootRepoService springBootRepoService) {
    this.springBootRepoService = springBootRepoService;
  }

  /**
   *  hottest repositories created in the last week
   *  
   * @param numberOfRepoToReturn default value is 10
   * @return list of  hottest repositories created in the last week
   */
  @RequestMapping(path =Constants.REPO_ENDPOINT, method = RequestMethod.GET)
  public ResponseEntity<List<ResponseRepoDTO>> getHotRepoList(@RequestParam(value =Constants.REPO_PARAM, defaultValue = "10") int numberOfRepoToReturn) {
	 
	 try {
        return ResponseEntity.ok().body(springBootRepoService.getHotRepoList(numberOfRepoToReturn));
    }catch (InvalidNumberOfAccountException e) {
    	mLOGGER.error("Exception Occured in getHotRepo: "+e.getMessage());
    	mLOGGER.debug("Bad Request: ");
    	throw new ResponseStatusException(
  	          HttpStatus.BAD_REQUEST, e.getMessage());
    }catch(Exception e) {
    	mLOGGER.error("Exception Occured in getHotRepo: "+e.getMessage());
    	mLOGGER.debug("Internal Server Error: ");
    	throw new ResponseStatusException(
  	          HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
    }
  }

}
