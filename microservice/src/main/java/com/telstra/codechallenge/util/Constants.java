package com.telstra.codechallenge.util;

public class Constants {
	
	public static final String REPO_ENDPOINT="/repo/";
	public static final String USER_ENDPOINT="/olduser/";
	
	public static final String USER_PARAM="numberOfAccountToReturn";
	public static final String REPO_PARAM="numberOfRepoToReturn";
}
