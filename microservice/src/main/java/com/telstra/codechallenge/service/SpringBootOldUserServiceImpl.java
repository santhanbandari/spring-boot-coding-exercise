package com.telstra.codechallenge.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.telstra.codechallenge.Response.ResponseOldUserDTO;
import com.telstra.codechallenge.controller.SpringBootRepoController;
import com.telstra.codechallenge.exceptions.InvalidNumberOfAccountException;
import com.telstra.codechallenge.exceptions.InvalidUrlException;
import com.telstra.codechallenge.model.Item;
import com.telstra.codechallenge.model.User;

/**
 * SpringBootOldUserService Implementation
 * 
 * @author Santhan.Bandari
 *
 */
@Service
public class SpringBootOldUserServiceImpl implements SpringBootOldUserService{

  private static final Logger mLOGGER = Logger.getLogger(SpringBootOldUserServiceImpl.class);
  @Value("${user.base.url}")
  private String userBaseUrl;
  
  private RestTemplate restTemplate;

  public SpringBootOldUserServiceImpl(RestTemplate restTemplate) {
    this.restTemplate = restTemplate;
  }

  /**
   * 
   * Method return oldest user accounts with zero followers
   * 
   * @param numberOfAccountToReturn
   * @return List of ResponseOldUserDTO
   * @throws InvalidNumberOfAccountException 
   * @throws InvalidUrlException
   */
  public List<ResponseOldUserDTO> getOldUserList(int numberOfAccountToReturn) throws InvalidNumberOfAccountException,InvalidUrlException {
	  mLOGGER.debug("In getOldUserList: Using  UserBaseUrl: "+userBaseUrl);
	 
	  if(numberOfAccountToReturn<=0) {
		  throw new InvalidNumberOfAccountException("Invalid Parameter Number of Repositories");
	  }
	  
	  List<ResponseOldUserDTO> listOldUserDTO = new ArrayList<ResponseOldUserDTO>(); 
	  User forObject=null;
	  try {
		  forObject = restTemplate.getForObject(userBaseUrl, User.class);
	  }catch(Exception e) {
		  throw new InvalidUrlException("Internal Server Error");
	  }
	  if(forObject.getItems()!= null) {
		  List<Item> items = forObject.getItems();
		  numberOfAccountToReturn=Math.min(numberOfAccountToReturn, items.size());
		  for(int i=0; i<numberOfAccountToReturn; i++) {
			  ResponseOldUserDTO responseOldUserDTO = new ResponseOldUserDTO();
			  Item item=items.get(i);
			  responseOldUserDTO.setId(item.getId());
			  responseOldUserDTO.setLogin(item.getLogin());
			  responseOldUserDTO.setHtml_url(item.getHtml_url());
			 
			  listOldUserDTO.add(responseOldUserDTO);
		  }
		  return listOldUserDTO;	
	  }
		  
	  mLOGGER.debug("In getOldUserList:No Old User was Found, Returning Empty list:");
	  return listOldUserDTO;	
	 

	}
	

}
