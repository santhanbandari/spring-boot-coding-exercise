package com.telstra.codechallenge.service;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.telstra.codechallenge.Response.ResponseRepoDTO;
import com.telstra.codechallenge.exceptions.InvalidNumberOfAccountException;
import com.telstra.codechallenge.exceptions.InvalidUrlException;
import com.telstra.codechallenge.model.Item;
import com.telstra.codechallenge.model.User;
import com.telstra.codechallenge.quotes.Quote;

/**
 * SpringBootRepoService Implementation
 * @author Santhan.Bandari
 *
 */
@Service
public class SpringBootRepoServiceImpl implements SpringBootRepoService{
	private static final Logger mLOGGER = Logger.getLogger(SpringBootRepoServiceImpl.class);
	
  @Value("${quotes.base.url}")
  private String quotesBaseUrl;
  
  private RestTemplate restTemplate;

  public SpringBootRepoServiceImpl(RestTemplate restTemplate) {
    this.restTemplate = restTemplate;
  }
  
  @Value("${repo.base.url}")
  private String repoBaseUrl;
  
  @Value("${repo.base.url.sort.order}")
  private String sorting_order;
  
  
  /**
   * Returns an Git Preository Url  for repo base Url.
   * @return String Git Repo.
   */
  public String getRepoUrl() {
	  
	  DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	  Calendar cal = Calendar.getInstance();
	  cal.add(Calendar.DATE, -7);
	  Date currDate = cal.getTime(); 
	  String fromdate = dateFormat.format(currDate);
	  
	  StringBuilder url = new StringBuilder();
	  url.append(repoBaseUrl+"?q=created:>"+fromdate+sorting_order);
	    
	  mLOGGER.debug("In getRepoUrl: Url: "+url.toString());
	  return url.toString();
	  
  }

  
  /**
   * Returns an list of ResponseRepoDTO.
   *
   *
   * @return - a ResponseRepoDTO List
   */
  public List<ResponseRepoDTO> getHotRepoList(int numberOfAccountToReturn) throws InvalidNumberOfAccountException,InvalidUrlException {
	  String repoUrl=getRepoUrl();
	  mLOGGER.debug("Using Repo Url: "+repoUrl);
	  if(numberOfAccountToReturn<=0) {
		  throw new InvalidNumberOfAccountException("Invalid Parameter Number of Repositories");
	  }
	  List<ResponseRepoDTO> listRepoDTO = new ArrayList<ResponseRepoDTO>(); 
	  User forObject=null;
	  try {
		  forObject = restTemplate.getForObject(repoUrl, User.class);
	  }catch(Exception e) {
		  throw new InvalidUrlException("Internal Server Error");
	  }
	  if(forObject.getItems()!= null) {
		  List<Item> items = forObject.getItems();
		  numberOfAccountToReturn=Math.min(numberOfAccountToReturn, items.size());
		  for(int i=0; i<numberOfAccountToReturn; i++) {
			  ResponseRepoDTO responseUserDTO = new ResponseRepoDTO();
			  Item item=items.get(i);
			  responseUserDTO.setName(item.getName());
			  responseUserDTO.setLanguage(item.getLanguage());
			  responseUserDTO.setDescription(item.getDescription());
			  responseUserDTO.setHtml_url(item.getHtml_url());
			  responseUserDTO.setWatchers_count(item.getWatchers_count());
			  listRepoDTO.add(responseUserDTO);
		  }
		  return listRepoDTO;
	  }
	  mLOGGER.debug("In getHotReporList:No Repo was Found, Returning Empty list:");
	  return listRepoDTO;	
  }
  


}
