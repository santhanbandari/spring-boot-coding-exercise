package com.telstra.codechallenge.service;

import java.util.List;

import com.telstra.codechallenge.Response.ResponseOldUserDTO;
import com.telstra.codechallenge.exceptions.InvalidNumberOfAccountException;
import com.telstra.codechallenge.exceptions.InvalidUrlException;
/**
 * SpringBootOldUserService Interface
 * @author Santhan.Bandari
 *
 */
public interface SpringBootOldUserService {
	/**
	 * oldest user accounts with zero followers
	 * 
	 * @param numberOfAccountToReturn
	 * @return
	 * @throws InvalidNumberOfAccountException
	 * @throws InvalidUrlException
	 */
	 public List<ResponseOldUserDTO> getOldUserList(int numberOfAccountToReturn) throws InvalidNumberOfAccountException, InvalidUrlException;
}
