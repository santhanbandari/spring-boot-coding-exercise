package com.telstra.codechallenge.service;

import java.util.List;

import com.telstra.codechallenge.Response.ResponseRepoDTO;
import com.telstra.codechallenge.exceptions.InvalidNumberOfAccountException;
import com.telstra.codechallenge.exceptions.InvalidUrlException;

/**
 * SpringBootRepoService Interface
 * @author Santhan.Bandari
 *
 */
public interface SpringBootRepoService {
	public List<ResponseRepoDTO> getHotRepoList(int numberOfAccountToReturn) throws InvalidNumberOfAccountException, InvalidUrlException;
}
